namespace :connection do
  desc "After per 1 minutes, the service will check online connection in redis by last updated time"
  task check_connection: :environment do
    keys = $redis.keys("*")
    keys.each do |key|
      status = $redis.get(key)
      if (Time.new.to_i - eval(status)[:last_updated]) > 60
        $redis.del(key)
        $redis.set(key, {last_updated:Time.new.to_i, online: false})
      end
    end
  end

end
