Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  post 'account_activation', to: "account_activation#activate"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post "/api/users/create", to: "user#create"
  post "/api/users/login", to: "user#login"

  get "/api/users/get", to: "user#get"
  post "/api/users/get", to: "user#get_user"
  post "/api/users/process_relationship", to: "user#process_relationship"
  post "/api/users/edit_profile", to: "user#edit_profile"

  get "/api/conversation_list/get", to: "conversation_list#get"

  post "/api/conversation/search", to: "conversation#search_conversation"
  post "/api/conversation/select_user", to: "conversation#select_user"

  post "/api/message/send", to: "message#received_message"
  post "/api/message/get_messages", to: "message#get_messages"
  post "/api/message/upload_photo", to: "message#upload_photo"
  post "/api/message/delete_message", to: "message#delete_message"

  get "/api/resources/photo", to: "resources#get_photo"

  get "/test", to: "resources#test"

  get "/api/connection/get", to: "connection#get"


  get "/api/admin/get_all_users", to: "admin#get_all_users"
  get "/api/admin/get_all_conversations", to: "admin#get_all_conversations"
  post "/api/admin/get_all_messages_by_conversation_id", to: "admin#get_all_messages_by_conversation_id"
  post "/api/admin/edit_user_profile", to: "admin#edit_user_profile"
  post "/api/admin/edit_conversation", to: "admin#edit_conversation"
  get "/api/admin/get_users_reports", to: "admin#get_users_reports"
  get "/api/admin/messages_count", to: "admin#messages_count"
  get "/api/admin/conversations_count", to: "admin#conversations_count"
  get "/api/admin/get_conversation_in_28_days", to: "admin#get_conversation_in_28_days"
  get "/api/admin/get_message_in_28_days", to: "admin#get_messages_in_28_days"

end
