
server '52.163.189.179', roles: [:web, :app, :db]

set :repo_url,        'git@gitlab.com:littleBoy1945/messenger_rails.git'

set :application,     'RailsApp'
set :user,            'anhnguyenhust'
set :puma_threads,    [4, 16]
set :puma_workers,    0
set :migration_role, :app


# Don't change these unless you know what you're doing
set :pty,             true
set :use_sudo,        false
set :stage,           :production
set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/#{fetch(:application)}"
set :puma_bind,       "unix://home/anhnguyenhust/RailsApp/shared/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "home/anhnguyenhust/RailsApp/shared/tmp/pids/puma.state"
set :puma_pid,        "home/anhnguyenhust/RailsApp/shared/tmp/pids/puma.pid"
set :puma_access_log, "/home/anhnguyenhust/RailsApp/current/log/puma.error.log"
set :puma_error_log,  "/home/anhnguyenhust/RailsApp/current/log/puma.access.log"
set :ssh_options,     { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/messenger.pem) }
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord


namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir home/anhnguyenhust/RailsApp/shared/tmp/sockets -p"
      execute "mkdir home/anhnguyenhust/RailsApp/shared/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end

namespace :deploy do
  # desc "Make sure local git is in sync with remote."
  # task :check_revision do
  #   on roles(:app) do
  #     unless `git rev-parse HEAD` == `git rev-parse origin/master`
  #       puts "WARNING: HEAD is not the same as origin/master"
  #       puts "Run `git push` to sync changes."
  #       exit
  #     end
  #   end
  # end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  # before :starting,     :check_revision
  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  after  :finishing,    :restart
  
end

append :linked_files, "config/master.key"

namespace :deploy do
  namespace :check do
    before :linked_files, :set_master_key do
      on roles(:app), in: :sequence, wait: 10 do
        unless test("[ -f #{shared_path}/config/master.key ]")
          upload! 'config/master.key', "#{shared_path}/config/master.key"
        end
      end
    end
  end
end