require 'test_helper'

class AccountActivationControllerTest < ActionDispatch::IntegrationTest
  test "should get activate" do
    get account_activation_activate_url
    assert_response :success
  end

end
