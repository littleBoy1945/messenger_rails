require 'user_helper'
include UserHelper

class User < ApplicationRecord

  has_many :user_conversations
  has_many :user_in_relationships
  has_many :conversations, :through => :user_conversations
  has_many :relationships, :through => :user_in_relationships

  after_validation :set_activated_false
  after_create :secure_password

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  VALID_NAME_REGEX = /\A[a-zA-Z]*$\z/

  validates :email, length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX },
                uniqueness: true, allow_nil: true
  validates :first_name, length: {minimum:1, maximum:10},
            format: {with: VALID_NAME_REGEX}
  validates :last_name, length: {minimum:1, maximum:10},
            format: {with: VALID_NAME_REGEX}
  validates :password_hashed, presence: true, length: { minimum: 6 ,maximum:12}, allow_nil: false
    # Remembers a user in the database for use in persistent sessions.

    # Activate account
  def activate
    update_columns(activated: true, activated_time: Time.zone.now)
  end

    # Send activation email
  def send_activation_email
      UserMailer.account_activation(self).deliver_now
  end

    # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  def password_reset_expire?
    reset_send_at < 2.hours.ago
  end

  def set_activated_false
    update_attribute(:activated, false)
  end

  def set_activated_true
    update_attribute(:activated, true)
  end

  def secure_password 
      
    random_secret_key = Digest::MD5.hexdigest(email.to_s + password_key.to_s)
    random_password_key = SecurePassword.generate_random_key_for_each_user
    concated_password = SecurePassword.pre_hashing(random_password_key,password_hashed)
    password_hashed_ = SecurePassword.hash_password(concated_password)
    update_columns(password_hashed: password_hashed_, password_key: random_password_key, secret_key: random_secret_key)
  end

end
