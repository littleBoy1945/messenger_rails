class Relationship < ApplicationRecord
  has_many :user_in_relationships
  has_many :users, :through => :user_in_relationships
end
