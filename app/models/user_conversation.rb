class UserConversation < ApplicationRecord

  has_many :messages
  belongs_to :user
  belongs_to :conversation

end
