class AdminController < ApplicationController

  skip_before_action :verify_authenticity_token

  def get_all_users
    if @current_user.role == 1
      users = User.all.where.not(user_id: @current_user.user_id)
      return render json:users.as_json(only: 
        [:user_id, :first_name, :last_name, :phone_number, :email, :birth, :other_name, :sex, :activated, :activated_time,:role])
    else
      return render json:{message: "Invalid role"}, status: 400
    end
  end

  def get_all_conversations
    if @current_user.role == 1
      conversations = Conversation.all
      return render json: conversations.as_json
    else
      return render json:{message: "Invalid role"}, status: 400
    end
  end

  def get_all_messages_by_conversation_id
    if @current_user.role == 1
      conversation_id = params[:conversation_id]
      user_conversation_ids = UserConversation.where(conversation_id: conversation_id).pluck(:user_conversation_id)
      messages = Message.where(user_conversation_id: user_conversation_ids)
      return render json: messages
    else
      return render json: {message: "Invalid role"}, status: 400
    end
  end

  def edit_user_profile
    if @current_user.role == 1
      user_id = params[:user_id]
      update = User.where(user_id: user_id).first.update_columns(first_name: user_params[:first_name], last_name: user_params[:last_name], other_name: user_params[:other_name], phone_number: user_params[:phone_number], role: user_params[:role])
      if update == true
        return render json: {message: "success"}  
      else
        return render json: {message: "error"}, status: 500
      end
    else  
      return render json: {messange: "Invalid role"}, status: 400
    end
  end
  

  def edit_conversation
    if @current_user.role == 1
      conversation_id = params[:conversation_id]
      update = Conversation.where(conversation_id: conversation_id).first.update_columns(title: conversation_params[:title])
    else
      return render json: {message: "Invalid role"}, status: 400
    end
  end

  def get_users_reports
    if @current_user.role == 1
      users_all = User.all.pluck(:user_id, :activated, :sex,:role)
      number_of_users = users_all.length()
      male_user_array = users_all.select do |user|
        user[2] == "male"
      end
      male_user_count = male_user_array.length()
      activated_users_array = users_all.select do |user|
        user[1] == true
      end
      activated_users_count = activated_users_array.length()
      user_online_count = 0
      user_list_redis = $redis.keys("*")
      user_list_redis.each do |user|
        if 
          eval($redis.get(user))[:online] == true
          user_online_count += 1
        end
      end
      return render json: {user_count: number_of_users, male_user: male_user_count, activated_user: activated_users_count, user_online_count: user_online_count}
    else
      return render json: {message: "Invalid role"}, status: 400
    end
  end

  def messages_count
    if @current_user.role == 1
      messages = Message.all.pluck(:message_id, :deleted, :type_of_messages)
      messages_count = messages.length()
      available_messages = messages.select do |message|
        message[1] == false
      end
      available_messages_count = available_messages.length()
      photos_message = messages.select do |message|
        message[2] == 2
      end
      photos_messages_count = photos_message.length()
      return render json: {messages_count: messages_count, available_messages: available_messages_count, photos_message: photos_messages_count}
    else
      return render json: {message: "Invalid role"}, status: 400
    end
  end

  def get_conversation_in_28_days
    if @current_user.role == 1
      conversations_created_at =  Conversation.where("created_at > ?", 28.days.ago).pluck(:created_at)
      counting = Array.new(4,0)
      current_time = Time.new.to_i
      conversations_created_at.each do |conv|
        time = conv.to_i
        sub_time = current_time - time
        if sub_time < 604800
          counting[0] += 1
        elsif sub_time < 1209600
          counting[1] += 1
        elsif sub_time < 1814400
          counting[2] += 1
        else
          counting[3] += 1
        end
      end
      return render json: {counting: counting}
    else
      return render json: {message: "Invalid role"}, status: 400
    end
  end

  def get_messages_in_28_days
    if @current_user.role == 1
      messages_created_at = Message.where("created_at > ?", 28.days.ago).pluck(:created_at)
      counting = Array.new(4,0)
      current_time = Time.new.to_i
      messages_created_at.each do |message|
        time = message.to_i
        sub_time = current_time - time
        if sub_time < 604800
          counting[0] += 1
        elsif sub_time < 1209600
          counting[1] += 1
        elsif sub_time < 1814400
          counting[2] += 1
        else
          counting[3] += 1
        end
      end
      return render json: {counting: counting}
    else 
      return render json: {message: "Invalid role"}, status: 400
    end
  end

  def conversations_count
    if @current_user.role == 1
      conversations = Conversation.all.length()
      return render json:{conversation_count: conversations}
    else
      return render json: {message: "Invalid role"}, status: 400
    end
  end

  private
  def conversation_params
    params.require(:admin).permit(:title)
  end

  def user_params
    params.require(:admin).permit(:first_name, :last_name, :other_name, :phone_number, :role)
  end

end
