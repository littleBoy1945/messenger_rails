class ConversationController < ApplicationController
  skip_before_action :verify_authenticity_token

  def search_conversation
    key = params[:key]
    blocked_user_id_arr = Array.new

    blocked_rel_ids = @current_user.relationships.
      where(status: 3).
      pluck(:relationship_id)

    blocked_user_ids = UserInRelationship.
      where(relationship_id: blocked_rel_ids).
      where.not(user_id: @current_user.user_id).
      where(accepted: true).
      pluck(:user_id)
    
    user_list = User.where("first_name like ?", "%#{key}%").
      where.not(user_id: blocked_user_id_arr)
    # user_list_after_filter = user_list.select do |element|
    #   !(element.user_id.eql? @current_user.user_id) and !(blocked_user_id_arr.include? element.user_id)
    # end
    render json: {users: user_list}
  end

  def select_user
    
    user_id = params[:user_id]
    users_select = User.where("user_id": user_id)
    conversation_list_of_curr_usr = UserConversation.where("user_id": @current_user.user_id)
    conversation_list_of_selc_usr = UserConversation.where("user_id": user_id)
    
    conversation_mapping_curr_usr = conversation_list_of_curr_usr.map {|n| n.conversation_id}
    conversation_mapping_selc_usr = conversation_list_of_selc_usr.map {|n| n.conversation_id}
    
    common_conversation_id = conversation_mapping_curr_usr & conversation_mapping_selc_usr

    if conversation_list_of_curr_usr.blank? or conversation_list_of_selc_usr.blank? or common_conversation_id.empty? 
      
      random_conversation_id = Digest::MD5.hexdigest(Time.new.to_s)
      conversation_name = users_select.blank? ? "New conversation is created by ".concat(@current_user.first_name): "Conversation of " + @current_user.first_name + " ".concat(@current_user.last_name) + " and " + users_select.first.first_name + " ".concat(users_select.first.last_name)
      conversation = Conversation.create([{conversation_id: random_conversation_id, title: conversation_name}])
        
      random_user_conversation_id_1 = Digest::MD5.hexdigest(Time.new.to_s)
      random_user_conversation_id_2 = Digest::MD5.hexdigest(Time.new.to_s.concat("1"))   
        
      conn_id = conversation.first.conversation_id

      random_relationship_id = Digest::MD5.hexdigest(Time.new.to_s)
      relationship = Relationship.create({relationship_id: random_relationship_id, status: 0})
      user_in_relationship_id_1 = Digest::MD5.hexdigest(Time.new.to_s)
      user_in_relationship_id_2 = Digest::MD5.hexdigest(Time.new.to_s + "1")
      user_in_relationships = UserInRelationship.create([{user_in_relationship_id: user_in_relationship_id_1,relationship_id: random_relationship_id,user_id: @current_user.user_id, accepted:false }, {user_in_relationship_id: user_in_relationship_id_2,relationship_id: random_relationship_id,user_id: user_id, accepted:false}])

      user_conversation = UserConversation.create([{user_conversation_id: random_user_conversation_id_1, user_id: @current_user.user_id, conversation_id: conn_id, deleted:false},{user_conversation_id: random_user_conversation_id_2, user_id: users_select.first.user_id, conversation_id: conn_id, deleted: false}])
      return render json: {conversation: conversation.first, user_selected: users_select.first, relationship: 0, curr_user_accepted_relationship: false}, status: 201
    end
    contact_user = User.where("user_id": user_id).first
    user_in_relationships = UserInRelationship.where("user_id": [@current_user.user_id, user_id])
    current_user_in_relationship_lst = user_in_relationships.select do |user|
      user.user_id == @current_user.user_id
    end
    contact_user_in_relationship_lst = user_in_relationships.select do |user|
      user.user_id == contact_user.user_id
    end
    current_user_in_relationship_id = current_user_in_relationship_lst.map { |user_in_relationship| user_in_relationship.relationship_id }
    contact_user_in_relationship_id = contact_user_in_relationship_lst.map { |user_in_relationship| user_in_relationship.relationship_id }
    common_relation = current_user_in_relationship_id & contact_user_in_relationship_id
    relationship_id = common_relation.first
    current_user_in_relationship = current_user_in_relationship_lst.select do |curr_usr|
      curr_usr.relationship_id == relationship_id
    end
    relationship = Relationship.where("relationship_id": relationship_id).first
    conn = Conversation.where("conversation_id": common_conversation_id.first).first
    return render json: {conversation: conn, user_selected: users_select.first,relationship: relationship.status, curr_user_accepted_relationship: current_user_in_relationship.first.accepted }, status: 200
    
  end

end
