class ConnectionController < ApplicationController
  skip_before_action :verify_authenticity_token

  def get
    $redis.set(@current_user.user_id, {last_updated:Time.new.to_i, online: true})
    render json: {message: "success"}
  end

end
