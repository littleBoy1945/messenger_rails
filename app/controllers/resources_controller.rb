class ResourcesController < ApplicationController
  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate

  def get_photo
    id = params[:id]
    type = id.split(".")[1]
    # file = File.open("public/messages/fc30808c5e2da2979a7c7c6aadc36e1e.png", "rb") {|file| file.read}
    send_file("public/messages/#{id}", type: "image/#{type}")
  end

  def test
    render json: {message: "test"}
  end

end
