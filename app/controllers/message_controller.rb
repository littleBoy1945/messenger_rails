require "message_helper"
include MessageHelper

class MessageController < ApplicationController

  skip_before_action :verify_authenticity_token

  def received_message
    conversation = params[:conversation]
    message = params[:message]
    user_conversations = UserConversation.where(conversation_id: conversation)
    
    if user_conversations.blank?
      return render json:{error: "Conversation not found !"}, status: 400
    else
      current_user_conversations = user_conversations.select do |usr|
        usr.user_id == @current_user.user_id
      end
      contact_user_conversations = user_conversations.select do |usr|
        usr.user_id != @current_user.user_id
      end
      contact_user_conversation_id = contact_user_conversations.first.user_id
      contact_user = User.find contact_user_conversation_id
      relationship_contact_user = contact_user.relationships
      relationship_contact_user_id = relationship_contact_user.map {|re| re.relationship_id}
      relationship_current_user = @current_user.relationships
      relationship_current_user_id = relationship_current_user.map {|re| re.relationship_id}
      common_relationship_id = (relationship_contact_user_id & relationship_current_user_id).first
      common_relationship = relationship_current_user.select do |rela|
        rela.relationship_id == common_relationship_id
      end

      if common_relationship.first.status == 1
        user_conversation_id = current_user_conversations.first.user_conversation_id
        random_message_id = Digest::MD5.hexdigest(Time.new.to_s)
        message_create = Message.new(message_id: random_message_id, user_conversation_id: user_conversation_id, content: message, deleted: false, type_of_messages: 1)
        message_create.save(validate: false)
        render json: {message: message_create}
      else
        return render json:{error: "You and this user is not friend !"}, status: 400
      end
      
    end
  end

  def get_messages
    conversation_id = params[:current_conversation]
    user_conversations = UserConversation.where("conversation_id": conversation_id).pluck(:user_conversation_id,:user_id)
    user_conversation_ids = user_conversations.map {|a| a[0]}
    user_ids = user_conversations.map {|a| a[1]}
    messages = Message.where("user_conversation_id": user_conversation_ids,"deleted": false)
    messages_list_1 = Array.new
    messages_list_2 = Array.new
    messages.each do |message|
      if message.user_conversation_id == user_conversation_ids[0]
        messages_list_1.push(message)
      else
        messages_list_2.push(message)
      end
    end
    messages_arr = Array.new
    messages_arr.push({messages:messages_list_1, user_id: user_ids[0]})
    messages_arr.push({messages:messages_list_2, user_id: user_ids[1]})
    return render json:{messages: messages_arr}
  end

  def upload_photo
    photo = params[:photo]
    conversation_id = params[:conversation_id]
    message_validation = ValidationPhoto.validate(photo)
    random_message_id = Digest::MD5.hexdigest(Time.new.to_s)
    user_conversation = UserConversation.where("conversation_id": conversation_id, "user_id": @current_user.user_id).first
    user_conversation_id = user_conversation.user_conversation_id
    message_create = Message.new(message_id: random_message_id, user_conversation_id: user_conversation_id,content: photo.content_type.split("/")[1], type_of_messages: 2, deleted: false)
    message_create.save(validate: false)
    if message_validation.empty?
      File.open("public/messages/#{random_message_id}.#{photo.content_type.split("/")[1]}", "wb") do |file|
        file.puts photo.read
      end
      render json:{ message: "success"}
    else
      render json:{ message: message_validation }, status: 400
    end
  end

  def delete_message
    message_id = params[:message_id]
    messages = Message.where("message_id": message_id)
    if messages.blank?
      return render json:{ message: "cannot find message"}, status: 400
    else 
      user_conversation = UserConversation.where("user_conversation_id": messages.first.user_conversation_id).first
      if user_conversation.user_id.eql? @current_user.user_id
        messages.first.update_attributes("deleted": true)
        return render json:{ message: "deleted message !"}
      else 
        return render json:{ message: "you're not the owner of this message !"}, status: 400
      end
    end

  end
  
end
