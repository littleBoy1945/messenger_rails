class ConversationListController < ApplicationController

  skip_before_action :verify_authenticity_token

  def get

    user = User.find @current_user.user_id
    render json:{conversation: user.conversations}

  end
end
