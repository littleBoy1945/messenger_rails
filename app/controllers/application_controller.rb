class ApplicationController < ActionController::Base

  before_action :authenticate

  private
   
    def authenticate
      token = get_token

      if token.blank?
        render json: {error: "Missing token"}, status: 401
        return
      end

      auth = Auth.decode(token) rescue nil

      unless auth
        render json: {error: "Invalid token"}, status: 401
        return
      end

      @current_user = User.where(user_id: auth["user_id"]).first

      unless @current_user
        render json: {error: "User not found"}, status: 400
      end
    end

    def logged_in?
      !!current_user
    end

    def current_user
      @current_user
    end

    def get_token
      request.env["HTTP_AUTHORIZATION"].scan(/Bearer(.*)$/).flatten.last
    end

end
