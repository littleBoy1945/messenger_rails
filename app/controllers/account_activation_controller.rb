require "account_activation_helper"
include AccountActivationHelper

class AccountActivationController < ApplicationController

  skip_before_action :verify_authenticity_token, :authenticate

  def activate
    user_id = params[:user_id]
    secret_key = params[:secret_key]
    @user = User.where(user_id:user_id, secret_key:secret_key)
    if @user.blank?
      render json: {status: "User not found"}, status: 401
    elsif
      @user[0].activate
      new_secret_key = RandomKey.generate_random_secret_key(@user[0].email, @user[0].password_key)
      # @user[0].update_attribute(:secret_key, new_secret_key)
      jwt_encode_value = Auth.issue(TokenHelper.create_token(@user[0].user_id, Time.new.to_i.to_s))
      render json: {status: "Success", jwt: jwt_encode_value}
    end
  end
end
