require 'user_helper'
include UserHelper
require "account_activation_helper"
include AccountActivationHelper

class UserController < ApplicationController

  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate, :only => [:create, :login]

  def create
    params_ = params[:params]
    password_tmp = params[:password]
    first_name = params[:firstName]
    last_name = params[:lastName]
    email = params[:email]
    birth = params[:birth]
    sex = params[:sex]
    random_user_id = Digest::MD5.hexdigest(email)[0,15].to_i(16).to_s
    @user = User.new(user_id: random_user_id, email:email, birth:birth, first_name:first_name, last_name:last_name, password_hashed:password_tmp, sex:sex)
    
    if @user.save
      render json: {status: "Pending"}
      @user.send_activation_email
    else
      render json: {status: "Unprocessable"}
    end
  end

  def login

    email_param = params[:email]
    password_param = params[:password]
    @user = User.select(:user_id, :first_name, :last_name, :email, :birth, :sex, :password_key, :password_hashed, :activated).find_by(email: email_param)
    # byebug
    if !@user
      return render json: {error: "Tên đăng nhập hoặc mật khẩu không đúng !"}, status: 401
    end
  
    if !@user.activated
      return render json: {error: "Tài khoản chưa được kích hoạt !"}, status: 401
    end
    password_key = @user.password_key
    concated_password = SecurePassword.pre_hashing(password_key, password_param)
    password_hashed_ = SecurePassword.hash_password(concated_password)
    if password_hashed_ === @user.password_hashed
      jwt_encode_value = Auth.issue(TokenHelper.create_token(@user.user_id, Time.new.to_i.to_s))
      user_response = {user_id: @user.user_id, first_name: @user.first_name, last_name: @user.last_name, email: @user.email, birth: @user.birth, sex: @user.sex}
      result = {jwt: jwt_encode_value, user_info: user_response}
      return render json: result
    else
      return render json: {error: "Tên đăng nhập hoặc mật khẩu không đúng !"}, status: 401
    end
  end

  def get
    render json: current_user.as_json(only: [
      :user_id, :first_name, :last_name, :other_name, :phone_number
    ])
  end

  def get_user  # get user with conversation id
    conversation_id = params[:conversation_id]
    user_conversation = UserConversation.where("conversation_id": conversation_id)
    if user_conversation.blank?
      return render json: {message: "This user is not in your conversation !"}, status: 400
    else 
      contact_user_conversation = user_conversation.select do |user|
        !(user.user_id.eql? @current_user.user_id)
      end
      contact_user_id = contact_user_conversation.first.user_id
      contact_user = User.where("user_id": contact_user_id).first
      user_in_relationships = UserInRelationship.where("user_id": [@current_user.user_id, contact_user.user_id])
      current_user_in_relationship_lst = user_in_relationships.select do |user|
        user.user_id == @current_user.user_id
      end
      contact_user_in_relationship_lst = user_in_relationships.select do |user|
        user.user_id == contact_user.user_id
      end

      if current_user_in_relationship_lst.blank? or contact_user_in_relationship_lst.blank?
        random_relationship_id = Digest::MD5.hexdigest(Time.new.to_s)
        relationship = Relationship.create({relationship_id: random_relationship_id, status: 0})
        user_in_relationship_id_1 = Digest::MD5.hexdigest(Time.new.to_s)
        user_in_relationship_id_2 = Digest::MD5.hexdigest(Time.new.to_s + "1")
        user_in_relationships = UserInRelationship.create([{user_in_relationship_id: user_in_relationship_id_1,relationship_id: random_relationship_id,user_id: @current_user.user_id, accepted:false }, {user_in_relationship_id: user_in_relationship_id_2,relationship_id: random_relationship_id,user_id: contact_user.user_id, accepted: false}])
        return render json: {user: contact_user, relationship: 0, curr_user_accepted_relationship: false}
      end

      current_user_in_relationship_id = current_user_in_relationship_lst.map { |user_in_relationship| user_in_relationship.relationship_id }
      contact_user_in_relationship_id = contact_user_in_relationship_lst.map { |user_in_relationship| user_in_relationship.relationship_id }
      common_relation = current_user_in_relationship_id & contact_user_in_relationship_id

      if common_relation.blank?
        random_relationship_id = Digest::MD5.hexdigest(Time.new.to_s)
        relationship = Relationship.create({relationship_id: random_relationship_id, status: 0})
        user_in_relationship_id_1 = Digest::MD5.hexdigest(Time.new.to_s)
        user_in_relationship_id_2 = Digest::MD5.hexdigest(Time.new.to_s + "1")
        user_in_relationships = UserInRelationship.create([{user_in_relationship_id: user_in_relationship_id_1,relationship_id: random_relationship_id,user_id: @current_user.user_id, accepted:false }, {user_in_relationship_id: user_in_relationship_id_2,relationship_id: random_relationship_id,user_id: contact_user.user_id, accepted:false}])
        return render json: {user: contact_user, relationship: 0, curr_user_accepted_relationship: false}

      else
        relationship_id = common_relation.first
        current_user_in_relationship = current_user_in_relationship_lst.select do |curr_usr|
          curr_usr.relationship_id == relationship_id
        end
        relationship = Relationship.where("relationship_id": relationship_id).first
        return render json: {user: contact_user, relationship: relationship.status, curr_user_accepted_relationship: current_user_in_relationship.first.accepted }
      end
    end
  
  end

  def process_relationship
    option = params[:option]
    contact_user_id = params[:user_id]
    user_in_relationships = UserInRelationship.where("user_id": [@current_user.user_id, contact_user_id])
    current_user_in_relationship_lst = user_in_relationships.select do |user|
      user.user_id == @current_user.user_id
    end
    contact_user_in_relationship_lst = user_in_relationships.select do |user|
      user.user_id == contact_user_id
    end
    current_user_in_relationship_id = current_user_in_relationship_lst.map { |user_in_relationship| user_in_relationship.relationship_id }
    contact_user_in_relationship_id = contact_user_in_relationship_lst.map { |user_in_relationship| user_in_relationship.relationship_id }
    common_relation = current_user_in_relationship_id & contact_user_in_relationship_id
    relationship_id = common_relation.first
    relationship = Relationship.where("relationship_id": relationship_id).first
    relationship_status = relationship.status
    
    option = option.to_i
    case option
    when 1
      if [1,2,3].include? relationship_status
        return render json: {message: "Cannot execute adding a friend !"}, status: 400
      else
        relation_case_1 = Relationship.where("relationship_id":relationship_id).first
        relation_case_1.update_attribute(:status, 2)
        current_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": @current_user.user_id).first
        contact_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": contact_user_id).first
        current_user_in_relationship.update_attribute(:accepted, true)
        contact_user_in_relationship.update_attribute(:accepted, false)
        return render json: {message: "success", relationship_status: 2, curr_user_accepted_relationship:true}
      end
    
    when 2
      relation_case_2 = Relationship.where("relationship_id":relationship_id).first
      relation_case_2.update_attribute(:status, 3)
      current_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": @current_user.user_id).first
      contact_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": contact_user_id).first
      current_user_in_relationship.update_attribute(:accepted, true)
      contact_user_in_relationship.update_attribute(:accepted, false)
      return render json: {message: "success", relationship_status: 3, curr_user_accepted_relationship:true}
    
    when 3
      relation_case_3 = Relationship.where("relationship_id":relationship_id).first
      current_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": @current_user.user_id).first
      contact_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": contact_user_id).first
      is_friend = current_user_in_relationship.accepted & contact_user_in_relationship.accepted & (relationship_status == 1)
      
      if is_friend == true
        relation_case_3.update_attribute(:status, 4)
        current_user_in_relationship.update_attribute(:accepted, false)
        contact_user_in_relationship.update_attribute(:accepted, false)
        return render json: {message: "success", relationship_status: 4, curr_user_accepted_relationship:false}
      else
        return render json: {message: "Cannot execute unfriend !"}, status: 400
      end
      
    when 4
      relation_case_4 = Relationship.where("relationship_id":relationship_id).first
      current_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": @current_user.user_id).first
      contact_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": contact_user_id).first
      ready_for_accept_invite = (relationship_status == 2) & (current_user_in_relationship.accepted == false) & (contact_user_in_relationship.accepted == true)
      if ready_for_accept_invite == true
        relation_case_4.update_attribute(:status, 1)
        current_user_in_relationship.update_attribute(:accepted, true)
        return render json: {message: "success", relationship_status: 1, curr_user_accepted_relationship:true}
      else
        return render json: {message: "Cannot execute accepting invite request !"}, status: 400
      end
    
    when 5
      if relationship_status == 3
        relation_case_5 = Relationship.where("relationship_id":relationship_id).first
        current_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": @current_user.user_id).first
        relation_case_5.update_attribute(:status, 4)
        current_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": @current_user.user_id).first
        contact_user_in_relationship = UserInRelationship.where("relationship_id": relationship_id, "user_id": contact_user_id).first
        current_user_in_relationship.update_attribute(:accepted, false)
        contact_user_in_relationship.update_attribute(:accepted, false)
        return render json: {message: "success", relationship_status: 4, curr_user_accepted_relationship:true}
      else
        return render json: {message: "Cannot unblock this user !"}, status: 400
      end
    else
      return render json: {message: "Cannot make this relationship !"}, status: 400
    end

  end

  def edit_profile
    update = @current_user.update_columns(first_name: user_params[:first_name], last_name: user_params[:last_name], other_name: user_params[:other_name], phone_number: user_params[:phone_number])
    if update == true
      return render json: {message: "success"}
    else
      return render json: {message: "error"}, status: 500
    end
  end

  private
  def user_params
    params.require(:user).permit(:first_name, :last_name, :other_name, :phone_number)    
  end

end
