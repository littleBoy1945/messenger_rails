class UserMailer < ApplicationMailer

  default from: 'allmid66@gmail.com'
  layout 'mailer'

  def account_activation(user)
    @user = user
    user_id = user.user_id
    secret_key = user.secret_key
    @link_activation = ENV["CLIENT"] + "/account-activation?user_id=#{user_id}&secret_key=#{secret_key}"
    mail to: user.email, subject: "MESSENGER ACCOUNT ACTIVATION"
  end
end
