module ApplicationCable
  class Connection < ActionCable::Connection::Base

    identified_by :current_user_id

    def connect
      self.current_user_id = find_verified_user
    end

    protected
      def find_verified_user
        token = request.query_parameters()[:token].scan(/Bearer(.*)$/).flatten.last
        auth = Auth.decode(token) rescue nil
        if auth
          uid = auth["user_id"]
          if $redis.get(uid)
            $redis.set(uid, {last_updated:Time.new.to_i, online: true})
            return uid
          else
            if verified_user = User.where(user_id: auth["user_id"]).first
              $redis.set(uid, {last_updated:Time.new.to_i, online: true})
              return uid
            else
              # close connection
            end
          end
        else
          # close connection
        end
      end

  end
end
