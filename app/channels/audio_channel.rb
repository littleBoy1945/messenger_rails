class AudioChannel < ApplicationCable::Channel
  def subscribed
    if params[:conversation_id].present?
      conversation_id = params[:conversation_id]
      stream_from "audio_#{conversation_id}_channel"
      
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    conversation_id = data["conversation_id"]
    message = data["message"]
    AudioTransferJob.perform_now({message: message}, conversation_id)
  end
end
