module AccountActivationHelper

  class TokenHelper
    def self.create_token(field1, field2)
      token = Hash.new
      token["user_id"] = field1
      token["created_at"] = field2
      return token
    end
  end

end
