module MessageHelper 

  class ValidationPhoto
    def self.validate(photo)
      if photo.size > 5000000
        return "Kích thuớc ảnh không thể quá 5MB"
      end
       
      if !(["image/jpg", "image/jpeg", "image/png"].include? photo.content_type)
        return "Định dạng file không phù hợp"
      end
      return ""
    end
  end

end
