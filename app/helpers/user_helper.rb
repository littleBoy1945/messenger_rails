require 'digest'

module UserHelper

  class SecurePassword

    def self.generate_random_key_for_each_user
      (0...8).map { (65 + rand(26)).chr }.join
    end

    def self.pre_hashing(secure_key,password)
      password.concat(secure_key)
    end

    def self.hash_password(password)
      Digest::SHA512.hexdigest password
    end

  end

  class RandomKey
    def self.generate_random_secret_key(email,password_key)
      Digest::MD5.hexdigest(email + password_key)
    end
  end
end
