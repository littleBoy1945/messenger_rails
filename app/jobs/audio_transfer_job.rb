class AudioTransferJob < ApplicationJob
  queue_as :default

  def self.perform_now(message,conn_id)
    messageInt32Array = message[:message]
    conversation_id = conn_id

    ActionCable.server.broadcast("audio_#{conversation_id}_channel", messageInt32Array)
  end

end
