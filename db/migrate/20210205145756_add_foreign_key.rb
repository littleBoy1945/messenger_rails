class AddForeignKey < ActiveRecord::Migration[5.2]
  def change
    execute "ALTER TABLE messages ADD CONSTRAINT message_constraint FOREIGN KEY (user_conversation_id) REFERENCES user_conversations (user_conversation_id)" 
    execute "ALTER TABLE user_conversations ADD CONSTRAINT user_constraint FOREIGN KEY (user_id) REFERENCES users (user_id)"
    execute "ALTER TABLE user_conversations ADD CONSTRAINT conversation_constraint FOREIGN KEY (conversation_id) REFERENCES conversations (conversation_id)" 
  end
end
