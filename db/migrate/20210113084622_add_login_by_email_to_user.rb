class AddLoginByEmailToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :login_by_email, :boolean
  end
end
