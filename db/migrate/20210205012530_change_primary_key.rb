class ChangePrimaryKey < ActiveRecord::Migration[5.2]
  def change
    execute "ALTER TABLE messages DROP COLUMN id" 
    execute "ALTER TABLE users DROP COLUMN id"
    execute "ALTER TABLE conversations DROP COLUMN id"
    execute "ALTER TABLE user_conversations DROP COLUMN id"
  end
end
