class CreateUserConversations < ActiveRecord::Migration[5.2]
  def change
    create_table :user_conversations do |t|
      t.string :user_conversation_id
      t.string :user_id
      t.string :conversation_id
      t.boolean :deleted

      t.timestamps
    end
  end
end
