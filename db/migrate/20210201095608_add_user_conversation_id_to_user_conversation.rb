class AddUserConversationIdToUserConversation < ActiveRecord::Migration[5.2]
  def change
    add_column :user_conversations, :user_conversation_id, :string
  end
end
