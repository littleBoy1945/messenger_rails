class RemoveLoginByEmailFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :login_by_email, :boolean
  end
end
