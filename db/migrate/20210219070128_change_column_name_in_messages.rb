class ChangeColumnNameInMessages < ActiveRecord::Migration[5.2]
  def change
    rename_column :messages, :type, :type_of_messages
  end
end
