class CreateUserInRelationships < ActiveRecord::Migration[5.2]
  def change
    create_table :user_in_relationships, id: false do |t|
      t.string :user_in_relationship_id, null: false
      t.string :relationship_id
      t.string :user_id

      t.timestamps
    end
    execute "ALTER TABLE user_in_relationships ADD PRIMARY KEY (user_in_relationship_id);"
  end
end
