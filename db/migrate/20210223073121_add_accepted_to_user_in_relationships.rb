class AddAcceptedToUserInRelationships < ActiveRecord::Migration[5.2]
  def change
    add_column :user_in_relationships, :accepted, :boolean
  end
end
