class AddForeignKeyInUserAndRelationship < ActiveRecord::Migration[5.2]
  def change
    execute "ALTER TABLE user_in_relationships ADD CONSTRAINT user_relationship_constraint FOREIGN KEY (user_id) REFERENCES users (user_id)"
    execute "ALTER TABLE user_in_relationships ADD CONSTRAINT relationship_user_constraint FOREIGN KEY (relationship_id) REFERENCES relationships (relationship_id)" 
  end
end
