class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :user_id
      t.string :phone_number
      t.datetime :birth
      t.string :first_name
      t.string :last_name
      t.string :other_name
      t.string :email
      t.string :password_key
      t.text :password_hashed

      t.timestamps
    end
  end
end
