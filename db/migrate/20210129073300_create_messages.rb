class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.string :message_id
      t.string :user_conversation_id
      t.text :content
      t.boolean :deleted

      t.timestamps
    end
  end
end
