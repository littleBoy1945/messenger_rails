class SetPrimaryKey < ActiveRecord::Migration[5.2]
  def change
    execute "ALTER TABLE messages ADD PRIMARY KEY (message_id)" 
    execute "ALTER TABLE users ADD PRIMARY KEY (user_id)" 
    execute "ALTER TABLE conversations ADD PRIMARY KEY (conversation_id)" 
    execute "ALTER TABLE user_conversations ADD PRIMARY KEY (user_conversation_id)" 

  end
end
