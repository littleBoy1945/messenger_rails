class CreateRelationships < ActiveRecord::Migration[5.2]
  def change
    create_table :relationships, id: false do |t|
      t.string :relationship_id, null: false
      t.integer :status

      t.timestamps
    end
    execute "ALTER TABLE relationships ADD PRIMARY KEY (relationship_id);"

  end
end
