class AddActivatedTimeToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :activated_time, :datetime
  end
end
