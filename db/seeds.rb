# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
# conversation = Conversation.create([{conversation_id: "11566b6f3a60f328ab17d19c166c9ed6", title:"Conversation 1"}])
user_conversations = UserConversation.create([{user_conversation_id: "bb053b5112841c0a44b37569e0e605f2", user_id: "567678670289339301", conversation_id: "11566b6f3a60f328ab17d19c166c9ed6", deleted: false}])

{ user_conversation_id: "b0f096a793e5b2edd38b4120e90686db", user_id: "508116627621710302", conversation_id: "11566b6f3a60f328ab17d19c166c9ed6", deleted: false}